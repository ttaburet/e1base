# E1Base

This directory is used as a storage for the **E1Base** database. Which has been created by **Patrick BAS**.
The base is composed of images taken by the Micro 4/3 16 MP CMOS sensor from the **Z CAM E1** action camera.

This base is composed 202 paired scenes both availables captured at ISO100 and ISO200.

## Used in :
1. Tomáš Denemark, Patrick Bas, and Jessica Fridrich. "Natural steganography in JPEG compressed images." Electronic Imaging 2018.7 (2018): 1-10.
2. Taburet Théo, Théo Taburet, Patrick Bas, Jessica Fridrich, Wadih Sawaya. "Computing Dependencies between DCT Coefficients for Natural Steganography in JPEG Domain." Proceedings of the ACM Workshop on Information Hiding and Multimedia Security. ACM, 2019.
3. Taburet Théo, Patrick Bas, Wadih Sawaya, Jessica Fridrich. "A Natural Steganography Embedding Scheme Dedicated to Color Sensors in the JPEG Domain." 2019.
4. Taburet Théo, Patrick Bas, Wadih Sawaya, Jessica Fridrich. "Stéganographie naturelle pour images JPEG." 2019. 

